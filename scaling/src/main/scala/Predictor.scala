import breeze.linalg._
import breeze.numerics._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.json4s.jackson.Serialization
import org.rogach.scallop._

import scala.io.Source

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val train = opt[String](required = true)
  val test = opt[String](required = true)
  val k = opt[Int]()
  val json = opt[String]()
  val users = opt[Int]()
  val movies = opt[Int]()
  val separator = opt[String]()
  verify()
}

object Predictor {
  def main(args: Array[String]) {
    var conf = new Conf(args)

    // Remove these lines if encountering/debugging Spark
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val spark = SparkSession.builder.appName("1M Movie lens predictor")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val sc = spark.sparkContext

    println("Loading training data from: " + conf.train())
    val read_start = System.nanoTime
    val trainFile = Source.fromFile(conf.train())
    val trainBuilder = new CSCMatrix.Builder[Double](rows=conf.users(), cols=conf.movies())
    for (line <- trainFile.getLines) {
      val cols = line.split(conf.separator()).map(_.trim)
      trainBuilder.add(cols(0).toInt-1, cols(1).toInt-1, cols(2).toDouble)
    }
    val train = trainBuilder.result()
    trainFile.close
    val read_duration = System.nanoTime - read_start
    println("Read data in " + (read_duration/pow(10.0,9)) + "s")

    // conf object is not serializable, extract values that
    // will be serialized with the parallelize implementations
    val conf_users = conf.users()
    val conf_movies = conf.movies()
    val conf_k = conf.k()
    println("Compute kNN on train data...")

    println("Loading test data from: " + conf.test())
    val testFile = Source.fromFile(conf.test())
    val testBuilder = new CSCMatrix.Builder[Double](rows=conf.users(), cols=conf.movies())
    for (line <- testFile.getLines) {
      val cols = line.split(conf.separator()).map(_.trim)
      testBuilder.add(cols(0).toInt-1, cols(1).toInt-1, cols(2).toDouble)
    }
    val test = testBuilder.result()
    testFile.close

    println("Compute predictions on test data...")

    // val train = CSCMatrix(
    //   (1.0, 0.0, 2.0, 3.0),
    //   (0.0, 4.0, 5.0, 0.0),
    //   (6.0, 7.0, 0.0, 9.0)
    // )

    def preprocess(utils: Utils): (DenseVector[Double], CSCMatrix[Double], CSCMatrix[Double]) = {
      val usersAvg: DenseVector[Double] = utils.getUsersAvg(train)
      val normalizedDeviations: CSCMatrix[Double] = utils.getNormalizedDeviations(train, usersAvg)
      val partialSimilarities: CSCMatrix[Double] = utils.getPartialSimilarities(normalizedDeviations)
      (usersAvg, normalizedDeviations, partialSimilarities)
    }

    def predict(train: CSCMatrix[Double], user: Int, movie: Int, userAvg: Double,
                normalizedDeviations: CSCMatrix[Double], cosineSimilarities: CSCMatrix[Double]): Double = {
      val userWeightedAverage: Double = getUserWeightedAverage(train, user, movie, cosineSimilarities, normalizedDeviations)
      userAvg + userWeightedAverage * scale(userAvg + userWeightedAverage, userAvg)
    }

    def getUserWeightedAverage(train: CSCMatrix[Double], user:Int, movie:Int, cosineSimilarities: CSCMatrix[Double],
                               normalizedDeviations: CSCMatrix[Double]): Double = {
      val userSimilarities: SliceVector[(Int, Int), Double] = cosineSimilarities(user, 0 until cosineSimilarities.cols).t
      var numerator = 0.0
      var denominator = 0.0
      for (userV <- userSimilarities.activeKeysIterator) {
        if (train(userV, movie) != 0.0 ){
          numerator += userSimilarities(userV) * normalizedDeviations(userV, movie)
          denominator += math.abs(userSimilarities(userV))
        }
      }
      if (denominator > 0.0) numerator / denominator else 0.0
    }

    def scale(x: Double, userAvgRating: Double): Double = {
      if (x > userAvgRating)
        5 - userAvgRating
      else if (x < userAvgRating)
        userAvgRating - 1
      else
        1
    }

    val start = System.nanoTime()
    val utils = new Utils(conf_users, conf_movies)
    val (usersAvg, normalizedDeviations, partialSimilarities) = preprocess(utils)
    val partialSimilaritiesBroadcast = sc.broadcast(partialSimilarities)

    def topk(user: Int, k: Int): Array[(Int, Int, Double)] = {
      val partialSimilarities = partialSimilaritiesBroadcast.value
      val similarities = partialSimilarities * partialSimilarities(user, 0 until partialSimilarities.cols).t
      argtopk(similarities, k).map(v => (user, v, similarities(v))).toArray
    }
    
    val knnBuilder = new CSCMatrix.Builder[Double](rows=conf_users, cols=conf_users)
    val topks: Array[(Int, Int, Double)] = sc.parallelize(0 until conf_users).map(user => topk(user, conf_k+1)).collect().flatten
    topks.foreach(el => knnBuilder.add(el._1, el._2, el._3))
    val cosineSimilarities = knnBuilder.result()

    for (i <- 0 until cosineSimilarities.rows) {
      cosineSimilarities(i, i) = 0.0
    }
    val durationKnn = (System.nanoTime() - start) / 1000

    var MAE = 0.0
    for ((k, rating) <- test.activeIterator) {
      val user = k._1
      val movie = k._2
      val prediction = predict(train, user, movie, usersAvg(user), normalizedDeviations, cosineSimilarities)
      MAE += math.abs(rating - prediction)
    }
    val durationPreds = (System.nanoTime() - start) / 1000
    MAE /= test.activeSize
    

    // Save answers as JSON
    def printToFile(content: String,
                    location: String = "./answers.json") =
      Some(new java.io.PrintWriter(location)).foreach{
        f => try{
          f.write(content)
        } finally{ f.close }
      }
    conf.json.toOption match {
      case None => ;
      case Some(jsonFile) => {
        var json = "";
        {
          // Limiting the scope of implicit formats with {}
          implicit val formats = org.json4s.DefaultFormats

          val answers: Map[String, Any] = Map(
            "Q4.1.1" -> Map(
              "MaeForK=200" -> MAE  // Datatype of answer: Double
            ),
            // Both Q4.1.2 and Q4.1.3 should provide measurement only for a single run
            "Q4.1.2" ->  Map(
              "DurationInMicrosecForComputingKNN" -> durationKnn  // Datatype of answer: Double
            ),
            "Q4.1.3" ->  Map(
              "DurationInMicrosecForComputingPredictions" -> durationPreds // Datatype of answer: Double  
            )
            // Answer the other questions of 4.1.2 and 4.1.3 in your report
          )
          json = Serialization.writePretty(answers)
        }

        println(json)
        println("Saving answers in: " + jsonFile)
        printToFile(json, jsonFile)
      }
    }

    println("")
    spark.stop()
  }
}
