#!/usr/bin/bash
# TIMEFORMAT=%R
# for i in 1 2 3 4 5
# do
# 	exec 3>&1 4>&2
# 	duration=$( { time ( spark-submit --master "local[1]" target/scala-2.11/m3_312519-assembly-1.0.jar --train ../../data/ml-1m/ra.train --test ../../data/ml-1m/ra.test --k 200 --json scaling-1m.json --users 6040 --movies 3952 --separator "::" > /dev/null ) 1>&3 2>&4; } 2>&1 )  # Captures time only.
# 	exec 3>&- 4>&-
# done
for i in 1 2 3 4 5
do
	spark-submit --master "local[1]" target/scala-2.11/m3_312519-assembly-1.0.jar --train ../../data/ml-1m/ra.train --test ../../data/ml-1m/ra.test --k 200 --json scaling-1m$i.json --users 6040 --movies 3952 --separator "::"
done
