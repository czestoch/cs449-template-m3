import breeze.linalg
import breeze.linalg._
import breeze.numerics._
import org.json4s.jackson.Serialization
import org.rogach.scallop._

import scala.collection.immutable
import scala.io.Source

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val train = opt[String](required = true)
  val test = opt[String](required = true)
  val k = opt[Int]()
  val json = opt[String]()
  val users = opt[Int]()
  val movies = opt[Int]()
  val separator = opt[String]()
  verify()
}

object Predictor {
  def main(args: Array[String]) {
    println("")
    println("******************************************************")

    var conf = new Conf(args)

    println("Loading training data from: " + conf.train())
    val read_start = System.nanoTime
    val trainFile = Source.fromFile(conf.train())
    val trainBuilder = new linalg.CSCMatrix.Builder[Double](rows=conf.users(), cols=conf.movies())
    for (line <- trainFile.getLines) {
        val cols = line.split(conf.separator()).map(_.trim)
        trainBuilder.add(cols(0).toInt-1, cols(1).toInt-1, cols(2).toDouble)
    }
    val train = trainBuilder.result()
    trainFile.close
    val read_duration = System.nanoTime - read_start
    println("Read data in " + (read_duration/pow(10.0,9)) + "s")

    println("Compute kNN on train data...")

    println("Loading test data from: " + conf.test())
    val testFile = Source.fromFile(conf.test())
    val testBuilder = new linalg.CSCMatrix.Builder[Double](rows=conf.users(), cols=conf.movies())
    for (line <- testFile.getLines) {
        val cols = line.split(conf.separator()).map(_.trim)
        testBuilder.add(cols(0).toInt-1, cols(1).toInt-1, cols(2).toDouble)
    }
    val test = testBuilder.result()
    testFile.close

    println("Compute predictions on test data...")
    def preprocess(utils: Utils): (DenseVector[Double], CSCMatrix[Double], CSCMatrix[Double]) = {
      val usersAvg: DenseVector[Double] = utils.getUsersAvg(train)
      val normalizedDeviations: CSCMatrix[Double] = utils.getNormalizedDeviations(train, usersAvg)
      val partialSimilarities: CSCMatrix[Double] = utils.getPartialSimilarities(normalizedDeviations)
      (usersAvg, normalizedDeviations, partialSimilarities)
    }

    def topk(user: Int, partialSimilarities: CSCMatrix[Double], k: Int): Array[(Int, Int, Double)] = {
      val similarities = partialSimilarities * partialSimilarities(user, 0 until partialSimilarities.cols).t
      argtopk(similarities, k).map(v => (user, v, similarities(v))).toArray
    }

    def evaluateTest(utils: Utils, usersAvg: DenseVector[Double],
                     normalizedDeviations: CSCMatrix[Double], cosineSimilarities: CSCMatrix[Double]) = {
      var mae = 0.0
      for ((coords, rating) <- test.activeIterator) {
        val user = coords._1
        val movie = coords._2
        val prediction = utils.predict(user, movie, usersAvg, train, normalizedDeviations, cosineSimilarities)
        mae += math.abs(rating - prediction)
      }
      mae / test.activeSize.toDouble
    }

    def getSimilarities(utils: Utils, k: Int): (DenseVector[Double], CSCMatrix[Double], CSCMatrix[Double]) = {
      val numUsers = conf.users()
      val (usersAvg, normalizedDeviations, partialSimilarities) = preprocess(utils)
      val knnBuilder = new CSCMatrix.Builder[Double](rows = numUsers, cols = numUsers)
      val topks: immutable.IndexedSeq[(Int, Int, Double)] = (0 until numUsers).flatMap(user => topk(user, partialSimilarities, k + 1))
      topks.foreach(el => knnBuilder.add(el._1, el._2, el._3))
      val cosineSimilarities = knnBuilder.result()
      for (i <- 0 until cosineSimilarities.rows) {
        cosineSimilarities(i, i) = 0.0
      }
      (usersAvg, normalizedDeviations, cosineSimilarities)
    }
    var MAEs: Map[Int, Double] = Map.empty
    //    MAEs += (100->1) MAEs += (200->1)
    val utils = new Utils(conf.users(), conf.movies())
    for (k <- Array(100, 200)){
      val (usersAvg, normalizedDeviations, cosineSimilarities) = getSimilarities(utils, k)
      val mae = evaluateTest(utils, usersAvg, normalizedDeviations, cosineSimilarities)
      MAEs += (k -> mae)
    }

    var durations: List[Long] = List()
    for (_ <- 1 to 5){
      val duration = utils.time(getSimilarities(utils, 200))._2
      durations = duration :: durations
    }
    val knnMean = DescriptiveStats.mean(durations)
    val knnStddev = DescriptiveStats.stdDev(durations)
    val knnMin = durations.min
    val knnMax = durations.max

    durations = List()
    for (_ <- 1 to 5){
      val duration = utils.time({
        val (usersAvg, normalizedDeviations, cosineSimilarities) = getSimilarities(utils, 200)
        for ((coords, _) <- test.activeIterator) {
          val user = coords._1
          val movie = coords._2
          utils.predict(user, movie, usersAvg, train, normalizedDeviations, cosineSimilarities)
        }
      })._2
      durations = duration :: durations
    }
    val predMean = DescriptiveStats.mean(durations)
    val predStddev = DescriptiveStats.stdDev(durations)
    val predMin = durations.min
    val predMax = durations.max

    //  Save answers as JSON
    def printToFile(content: String,
                    location: String = "./answers.json") =
      Some(new java.io.PrintWriter(location)).foreach{
        f => try{
          f.write(content)
        } finally{ f.close }
    }
    conf.json.toOption match {
      case None => ;
      case Some(jsonFile) => {
        var json = "";
        {
          // Limiting the scope of implicit formats with {}
          implicit val formats = org.json4s.DefaultFormats

          val answers: Map[String, Any] = Map(
            "Q3.3.1" -> Map(
              "MaeForK=100" -> MAEs(100), // Datatype of answer: Double
              "MaeForK=200" -> MAEs(200)  // Datatype of answer: Double
            ),
            "Q3.3.2" ->  Map(
              "DurationInMicrosecForComputingKNN" -> Map(
                "min" -> knnMin,  // Datatype of answer: Double
                "max" -> knnMax, // Datatype of answer: Double
                "average" -> knnMean, // Datatype of answer: Double
                "stddev" -> knnStddev // Datatype of answer: Double
              )
            ),
            "Q3.3.3" ->  Map(
              "DurationInMicrosecForComputingPredictions" -> Map(
                "min" -> predMin,  // Datatype of answer: Double
                "max" -> predMax, // Datatype of answer: Double
                "average" -> predMean, // Datatype of answer: Double
                "stddev" -> predStddev // Datatype of answer: Double
              )
            )
            // Answer the Question 3.3.4 exclusively on the report.
           )
          json = Serialization.writePretty(answers)
        }

        println(json)
        println("Saving answers in: " + jsonFile)
        printToFile(json, jsonFile)
      }
    }

    println("")
  }
}
