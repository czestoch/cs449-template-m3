import breeze.linalg.{CSCMatrix, DenseVector, SliceVector, argtopk}

class Utils(numUsers: Int, numMovies: Int) {

  def predict(user: Int, movie: Int, usersAvg: DenseVector[Double], train: CSCMatrix[Double],
              normalizedDeviations: CSCMatrix[Double], cosineSimilarities: CSCMatrix[Double]): Double = {
    val similarities: SliceVector[(Int, Int), Double] = cosineSimilarities(user, 0 until numUsers).t
    val userWeightedAverage = getUserWeightedAverage(train, similarities, normalizedDeviations, movie)
    val userAvg = usersAvg(user)
    userAvg + userWeightedAverage * scale(userAvg + userWeightedAverage, userAvg)

  }

  def getUserWeightedAverage(train: CSCMatrix[Double], similarities: SliceVector[(Int, Int), Double], 
                             normalizedDeviations: CSCMatrix[Double], movie: Int): Double = {
    var numerator = 0.0
    var denominator = 0.0
    for (userV <- similarities.activeKeysIterator) {
      if (train(userV, movie) != 0.0 ){
        numerator += similarities(userV) * normalizedDeviations(userV, movie)
        denominator += math.abs(similarities(userV))
      }
    }
    if (denominator > 0.0) numerator / denominator else 0.0
  }


  def getSimilarities(normalizedDeviations: CSCMatrix[Double]): CSCMatrix[Double] = {
    val ones = DenseVector.ones[Double](numMovies)
    val partialSimilaritiesDenominators = ((normalizedDeviations ^:^ 2.0) * ones) ^:^ 0.5
    val partialSimilaritiesBuilder = new CSCMatrix.Builder[Double](rows = numUsers, cols = numMovies)
    for ((k, v) <- normalizedDeviations.activeIterator) {
      val row = k._1
      val col = k._2
      val value = v / partialSimilaritiesDenominators(k._1)
      partialSimilaritiesBuilder.add(row, col, value)
    }
    val partialSimilarities = partialSimilaritiesBuilder.result()
    val cosineSimilarities = partialSimilarities * partialSimilarities.t
    for (i <- 0 until cosineSimilarities.rows) {
      cosineSimilarities(i, i) = 0.0
    }
    cosineSimilarities
  }

  def getPartialSimilarities(normalizedDeviations: CSCMatrix[Double]): CSCMatrix[Double] = {
    val ones = DenseVector.ones[Double](numMovies)
    val partialSimilaritiesDenominators = ((normalizedDeviations ^:^ 2.0) * ones) ^:^ 0.5
    val partialSimilaritiesBuilder = new CSCMatrix.Builder[Double](rows = numUsers, cols = numMovies)
    for ((k, v) <- normalizedDeviations.activeIterator) {
      val row = k._1
      val col = k._2
      val value = v / partialSimilaritiesDenominators(k._1)
      partialSimilaritiesBuilder.add(row, col, value)
    }
    partialSimilaritiesBuilder.result()
  }

  def getNormalizedDeviations(train: CSCMatrix[Double], usersAvg: DenseVector[Double]): CSCMatrix[Double] = {
    val normalizedDevBuilder = new CSCMatrix.Builder[Double](rows = numUsers, cols = numMovies)
    for ((k, v) <- train.activeIterator) {
      val row = k._1
      val col = k._2
      val value = (v - usersAvg(row)) / scale(v, usersAvg(row))
      normalizedDevBuilder.add(row, col, value)
    }
    normalizedDevBuilder.result()
  }

  def getUsersAvg(train: CSCMatrix[Double]): DenseVector[Double] = {
    val ones = DenseVector.ones[Double](numMovies)
    val usersSums: DenseVector[Double] = train * ones
    val usersNonZeroes: DenseVector[Double] = DenseVector.zeros[Double](numUsers)
    for (i <- 0 until numUsers) {
      val user: SliceVector[(Int, Int), Double] = train(i, 0 until numMovies).t
      for ((_, v) <- user.activeIterator) {
        if (v > 0.0) {
          usersNonZeroes(i) += 1
        }
      }
    }
    usersSums /:/ usersNonZeroes
  }

  def scale(x: Double, userAvgRating: Double): Double = {
    if (x > userAvgRating)
      5 - userAvgRating
    else if (x < userAvgRating)
      userAvgRating - 1
    else
      1
  }

  def time[R](block: => R): (R, Long) = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    (result, (t1 - t0) / 1000)
  }

}
