import org.rogach.scallop._
import org.json4s.jackson.Serialization
import breeze.linalg._
import breeze.numerics._
import scala.io.Source
import scala.collection.mutable.ArrayBuffer

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val json = opt[String]()
  verify()
}

object Economics {
  def main(args: Array[String]) {
    println("")
    println("******************************************************")

    var conf = new Conf(args)

//    Q1
    val ICCM7BuyingCost = 35000
    val ICCM7OperatingCost = 20.4
    val minDaysRentingICCM7 = ICCM7BuyingCost / ICCM7OperatingCost
    val minYearsRentingICCM7 = minDaysRentingICCM7 / 365

//    Q2
    val ICCM7Ram = 1536
    val containerPerRam = 0.012
    val ICCM7CPUs = 28
    val containerPerCPU = 0.088

    val dailyCostICContainer_Eq_ICCM7 = ICCM7Ram * containerPerRam + ICCM7CPUs * containerPerCPU
    val ratioICCM7_overContainer = ICCM7OperatingCost / dailyCostICContainer_Eq_ICCM7
    var containerCheaperThanICCM7 = false
    if (ratioICCM7_overContainer > 1) {
      containerCheaperThanICCM7 = true
    }

//    Q3
    val RPiRam = 8
    val RPiMinPower = 0.0108
    val RPiMaxPower = 0.054
//    4 RPis == 1vCPU on container
    val dailyCostICContainer_Eq_4Rpi4 = 4 * RPiRam * containerPerRam + containerPerCPU
    val Ratio4RPi_over_Container_MaxPower = 4 * RPiMaxPower / dailyCostICContainer_Eq_4Rpi4
    val Ratio4RPi_over_Container_MinPower = 4 * RPiMinPower / dailyCostICContainer_Eq_4Rpi4

//    Q4
    val RpiBuyingCost = 94.83
    val MinDaysRentingContainerToPay4RPis_MinPower = 4 * RpiBuyingCost / ((4 * RPiRam * containerPerRam + containerPerCPU) - 4*RPiMinPower)
    val MinDaysRentingContainerToPay4RPis_MaxPower = 4 * RpiBuyingCost / ((4 * RPiRam * containerPerRam + containerPerCPU) - 4*RPiMaxPower)

//    Q5
    val NbRPisForSamePriceAsICCM7 = (ICCM7BuyingCost / RpiBuyingCost).toInt
    val RatioTotalThroughputRPis_over_ThroughputICCM7 = NbRPisForSamePriceAsICCM7.toDouble / 4 / ICCM7CPUs
    val RatioTotalRAMRPis_over_RAMICCM7 = NbRPisForSamePriceAsICCM7.toDouble * RPiRam / ICCM7Ram

//    Q6
    val avgMoviesPerUser = 100.0
    val numNeighbours = 200.0
    val userMemoryBytes = (avgMoviesPerUser * 8.0 + numNeighbours * 32.0) / 8.0
    val NbUserPerGB = 1e9 / userMemoryBytes
    val NbUserPerRPi = RPiRam * 1e9 / 2 / userMemoryBytes
    val NbUserPerICCM7 = ICCM7Ram * 1e9 / 2 / userMemoryBytes


    // Save answers as JSON
    def printToFile(content: String,
                    location: String = "./answers.json") =
      Some(new java.io.PrintWriter(location)).foreach{
        f => try{
          f.write(content)
        } finally{ f.close }
    }
    conf.json.toOption match {
      case None => ;
      case Some(jsonFile) => {
        var json = "";
        {
          // Limiting the scope of implicit formats with {}
          implicit val formats = org.json4s.DefaultFormats

          val answers: Map[String, Any] = Map(
            "Q5.1.1" -> Map(
              "MinDaysOfRentingICC.M7" -> minDaysRentingICCM7, // Datatype of answer: Double
              "MinYearsOfRentingICC.M7" -> minYearsRentingICCM7 // Datatype of answer: Double
            ),
            "Q5.1.2" -> Map(
              "DailyCostICContainer_Eq_ICC.M7_RAM_Throughput" -> dailyCostICContainer_Eq_ICCM7, // Datatype of answer: Double
              "RatioICC.M7_over_Container" -> ratioICCM7_overContainer, // Datatype of answer: Double
              "ContainerCheaperThanICC.M7" -> containerCheaperThanICCM7 // Datatype of answer: Boolean
            ),
            "Q5.1.3" -> Map(
              "DailyCostICContainer_Eq_4RPi4_Throughput" -> dailyCostICContainer_Eq_4Rpi4, // Datatype of answer: Double
              "Ratio4RPi_over_Container_MaxPower" -> Ratio4RPi_over_Container_MaxPower, // Datatype of answer: Double
              "Ratio4RPi_over_Container_MinPower" -> Ratio4RPi_over_Container_MinPower, // Datatype of answer: Double
              "ContainerCheaperThan4RPi" -> false // Datatype of answer: Boolean
            ),
            "Q5.1.4" -> Map(
              "MinDaysRentingContainerToPay4RPis_MinPower" -> MinDaysRentingContainerToPay4RPis_MinPower, // Datatype of answer: Double
              "MinDaysRentingContainerToPay4RPis_MaxPower" -> MinDaysRentingContainerToPay4RPis_MaxPower // Datatype of answer: Double
            ),
            "Q5.1.5" -> Map(
              "NbRPisForSamePriceAsICC.M7" -> NbRPisForSamePriceAsICCM7, // Datatype of answer: Double
              "RatioTotalThroughputRPis_over_ThroughputICC.M7" -> RatioTotalThroughputRPis_over_ThroughputICCM7, // Datatype of answer: Double
              "RatioTotalRAMRPis_over_RAMICC.M7" -> RatioTotalRAMRPis_over_RAMICCM7 // Datatype of answer: Double
            ),
            "Q5.1.6" ->  Map(
              "NbUserPerGB" -> NbUserPerGB, // Datatype of answer: Double
              "NbUserPerRPi" -> NbUserPerRPi, // Datatype of answer: Double
              "NbUserPerICC.M7" -> NbUserPerICCM7 // Datatype of answer: Double
            )
            // Answer the Question 5.1.7 exclusively on the report.
           )
          json = Serialization.writePretty(answers)
        }

        println(json)
        println("Saving answers in: " + jsonFile)
        printToFile(json, jsonFile)
      }
    }

    println("")
  } 
}
